// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GhostGame/ChangeAffiliation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeChangeAffiliation() {}
// Cross Module References
	GHOSTGAME_API UClass* Z_Construct_UClass_UChangeAffiliation_NoRegister();
	GHOSTGAME_API UClass* Z_Construct_UClass_UChangeAffiliation();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_GhostGame();
// End Cross Module References
	void UChangeAffiliation::StaticRegisterNativesUChangeAffiliation()
	{
	}
	UClass* Z_Construct_UClass_UChangeAffiliation_NoRegister()
	{
		return UChangeAffiliation::StaticClass();
	}
	struct Z_Construct_UClass_UChangeAffiliation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UChangeAffiliation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GhostGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChangeAffiliation_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "ChangeAffiliation.h" },
		{ "ModuleRelativePath", "ChangeAffiliation.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UChangeAffiliation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UChangeAffiliation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UChangeAffiliation_Statics::ClassParams = {
		&UChangeAffiliation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UChangeAffiliation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UChangeAffiliation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UChangeAffiliation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UChangeAffiliation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChangeAffiliation, 3516545789);
	template<> GHOSTGAME_API UClass* StaticClass<UChangeAffiliation>()
	{
		return UChangeAffiliation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChangeAffiliation(Z_Construct_UClass_UChangeAffiliation, &UChangeAffiliation::StaticClass, TEXT("/Script/GhostGame"), TEXT("UChangeAffiliation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChangeAffiliation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
