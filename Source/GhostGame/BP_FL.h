// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BP_FL.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API UBP_FL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
