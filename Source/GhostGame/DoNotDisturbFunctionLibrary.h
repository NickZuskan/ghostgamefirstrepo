// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DoNotDisturbFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API UDoNotDisturbFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
