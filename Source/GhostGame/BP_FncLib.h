// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BP_FncLib.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API UBP_FncLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
