// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyBlueprintFunctionLibrary12.h"
#include "FunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API UFunctionLibrary : public UMyBlueprintFunctionLibrary12
{
	GENERATED_BODY()
	
};
